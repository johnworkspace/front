FROM node 
RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
	  git \
	&& rm -rf /var/lib/apt/lists/*
RUN npm install -g bower polymer-cli --unsafe-perm
EXPOSE 5500 
# Create app directory 
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ADD . /usr/src/app

#Paquetes necesarios
RUN bower install --allow-root

CMD ["polymer", "serve","--compile","never","--port","5500","-H","0.0.0.0"]